# Seapickle Pack

This repository contains the configuration from which the seapickle pack is generated.
The pack will be in the form of an [PrismMc](https://prismlauncher.org/)-Instance, that wraps [Packwiz](https://packwiz.infra.link/) which then manages the downloading and updating of the mods, based on a configuration, that is hosted here: https://seapicklepack.vercel.app/pack/pack.toml.

# Build Process

This repo is being continuously built by vercel on each push on main.
Vercel uses `vercel-install.sh` to install certain dependencies, and then
executes `npm run build`.

This will trigger a Gulp, that does a lot of things.
Noteworth is that it can handle .hbs handlebars templates.

It also builds two seperate profiles (each profile is configurable in `gulpfile.ts`). One profile with all mods enabled, and one profile that uses the 
`option` feature of Packwiz so that users are prompted with a mod selection screen of optional mods.

The mmc_instance points to the instance with all the mods - if the customizable version is wanted, edit the instance and
replace the URL under `Settings->Custom Command` with `https://seapicklepack.vercel.app/pack_custom/pack.toml`

# Folders

## `website`

The folder `website` contains a tiny little website with installation instructions.

## `modpack`

Contains the files for Packwiz

## `resourcepack`

Will be zipped and then copied to `resourcepacks/Seapickle.zip`, before Packwiz builds the pack - so that it is included in the pack.

## `mmc_instance`

Contains the files that will be included in the instance for PrismMC.
The most important part here is, that it sets the Prelaunch-Command to

```
PreLaunchCommand="$INST_JAVA" -jar packwiz-installer-bootstrap.jar {{packUrl}}
```

Which will trigger Packwiz everytime the modpack launches.
