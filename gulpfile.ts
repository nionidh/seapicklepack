import { dest, src, series, parallel, TaskFunction } from "gulp";
import through2 from "through2";
import zip from "gulp-zip";
import Handlebars from "handlebars";

const URL = "https://seapicklepack.vercel.app";

interface CommonOptions {
  revision: string,
  revisionLong: string,
  revisionMessage: string,
  instanceUrl: string,
  packUrl: string,
  isoBuildDate: string
}
interface TemplateOptions extends CommonOptions {
  enableOptionals: boolean
}
type Profile = {
  options: TemplateOptions
  dir: string
}
type Profiles = Record<string, Profile>;

const filterOptionals = (templateOptions: TemplateOptions) => through2.obj(function(chunk, enc,callback) {
    if(chunk.extname === ".optional") {
        if(templateOptions.enableOptionals === true){
            chunk.extname = "";
            this.push(chunk);
        }else{
            console.log("Skipping optional file: ", chunk);
        }
    }else{
        this.push(chunk);
    }
    callback();
})

const processTemplates = (templateOptions: TemplateOptions) =>
  through2.obj(function(chunk, enc, callback) {
    if (chunk.extname === ".hbs") {
      chunk.extname = "";
      const template = Handlebars.compile(chunk.contents.toString());
      const compiled = template(templateOptions);
      chunk.contents = Buffer.from(compiled, enc);
      this.push(chunk);
    } else {
      this.push(chunk);
    }
    callback();
  });

function parallelProfiles(profiles: Profiles, undertaker: (profile: Profile) => TaskFunction) {
  return parallel(Object.entries(profiles).map(([_, profile]) =>
    undertaker(profile)
  ));
}

function buildResourcePack(profile: Profile): TaskFunction {
  return () => {
    return src("resourcepack/**/*")
      .pipe(processTemplates(profile.options))
      .pipe(zip("Seapickle.zip"))
      .pipe(dest(`${profile.dir}/resourcepacks`));
  }
}

function copyPack(profile: Profile): TaskFunction {
  return () => {
    return src("modpack/**/*")
        .pipe(processTemplates(profile.options))
        .pipe(filterOptionals(profile.options))
        .pipe(dest(profile.dir));
  }
}

function buildPack(profile: Profile): TaskFunction {
  return (cb) => {
    console.log("Building Pack ", profile.dir);
    const packOutput = require("child_process")
      .execSync("../../packwiz refresh", { cwd: `${__dirname}/${profile.dir}` })
      .toString()
      .trim();
    console.log(packOutput);
    cb();
  }
}

function buildWebsite(profile: Profile): TaskFunction {
  return () => src("website/**/*").pipe(processTemplates(profile.options)).pipe(dest("build/"));
}

function buildMMCInstance(profile: Profile): TaskFunction {
  return () => src(["mmc_instance/**/*", "mmc_instance/.**/*"], {})
    .pipe(processTemplates(profile.options))
    .pipe(zip("Seapickle.zip"))
    .pipe(dest("build/"));
}

const COMMON_OPTIONS: CommonOptions = {
  instanceUrl: `${URL}/Seapickle.zip`,
  packUrl: `${URL}/pack/pack.toml`,
  revision: require("child_process")
    .execSync("git rev-parse --short HEAD")
    .toString()
    .trim(),
  revisionLong: require("child_process")
    .execSync("git rev-parse HEAD")
    .toString()
    .trim(),
  revisionMessage: require("child_process").execSync("git log -1 --pretty=%B").toString().trim(),
  isoBuildDate: new Date().toISOString()
}

const PROFILES: Profiles = {
  "pack": {
    options: {
      enableOptionals: false,
      ...COMMON_OPTIONS
    },
    dir: "build/pack"
  },
  "customizable": {
    options: {
      enableOptionals: true,
      ...COMMON_OPTIONS
    },
    dir: "build/pack_custom"
  }
};

exports.default = parallel(
  buildWebsite(PROFILES["pack"]),
  buildMMCInstance(PROFILES["pack"]),
  series(parallelProfiles(PROFILES, copyPack), parallelProfiles(PROFILES, buildResourcePack), parallelProfiles(PROFILES, buildPack))
);
